<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ObjectsController {
    private $pdo;
    
    private $room_id;
    private $survey_id;
    private $host_id;
    private $room_type;
    private $country;
    private $city;
    private $borough;
    private $neighborhood;
    private $reviews;
    private $overall_satisfaction;
    private $accommodates;
    private $bedrooms;
    private $bathrooms;
    private $price;
    private $minstay;
    private $last_modified;
    private $latitude;
    private $longitude;
    private $location;
    
    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
    
    function readAll($table, array $filter) {
        if ($filter['neighborhood'] && $filter['borough']) {
            $whereClousure = "WHERE neighborhood = ? AND borough = ?";
            $params = [$filter['neighborhood'], $filter['borough']];
        } elseif ($filter['neighborhood']) {
            $whereClousure = "WHERE neighborhood = ?";
            $params = [$filter['neighborhood']];
        } elseif ($filter['borough']) {
            $whereClousure = "WHERE borough = ?";
            $params = [$filter['borough']];
        } else {
            $whereClousure = "";
            $params = [];
        }
        $sql = "SELECT *
                FROM $table
                $whereClousure
                ORDER BY last_modified DESC 
                LIMIT 100";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
    
    function readOne($table, $id) {
        $sql = "SELECT *
                FROM $table
                WHERE room_id = ?";
        $params = [$id];
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
    
    function insert($table, array $insertData) {
        $peaces = [];
        $params = [];
        
        if ($insertData) {
            foreach ($insertData as $key => $value) {
                array_push($peaces, $key .  ' = ?');
                array_push($params, $value);
            }
            $insertStr = implode(',', $peaces);

            $sql = "INSERT INTO $table 
                    SET $insertStr";
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись добавлена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при добавлении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Данные не были переданы'
            ];
        }
        return $res;
    }
    
    function update($table, $id, array $insertData) {
        $peaces = [];
        $params = [];
        
        if ($insertData) {
            foreach ($insertData as $key => $value) {
                array_push($peaces, $key .  ' = ?');
                array_push($params, $value);
            }
            $insertStr = implode(',', $peaces);
            array_push($params, $id);
            $sql = "UPDATE $table 
                    SET $insertStr
                    WHERE room_id = ?";
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись обновлена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при обновлении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Данные не были переданы'
            ];
        }
        return $res;
    }
    
    function delete($table, $id) {
        if ($id) {
            $sql = "DELETE FROM $table
                    WHERE room_id = ?";
            $params = [$id];
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись удалена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при удалении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Не выбран элемент для удаления'
            ];
        }
        return $res;
    }
}