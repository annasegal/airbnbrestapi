<?php
require_once 'libs/objectsController.php';
$router = new Phroute\RouteCollector(new Phroute\RouteParser);
$router->get('/', function() { 
    return 'root';
});
$router->get('rooms', function() {
    $object = new ObjectsController(getPDOInstance());
    $filter = [
        'neighborhood' => $_REQUEST['neighborhood'],
        'borough'      => $_REQUEST['borough']
    ];
    return $object->readAll('las_vegas', $filter);  
});

$router->get('room/{id}', function($id) {
    $object = new ObjectsController(getPDOInstance());
    return $object->readOne('las_vegas', $id);
});

$router->post('room', function() {
    $object = new ObjectsController(getPDOInstance());
    return $object->insert('las_vegas', $_REQUEST);
});

$router->put('room/{id}', function($id) {
    $object = new ObjectsController(getPDOInstance());
    $post_vars = [];
    parse_str(file_get_contents("php://input"),$post_vars);
    return $object->update('las_vegas', $id, $post_vars);
});

$router->delete('room/{id}', function ($id) {
    global $validUsers;
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
        $user = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];

        if (key_exists($user, $validUsers) && $pass == $validUsers[$user]) {
            $object = new ObjectsController(getPDOInstance());
            return $object->delete('las_vegas', $id);
        } 
    }
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    return ['status' => 'error','message' => 'Not authorized'];

});

$dispatcher = new Phroute\Dispatcher($router);
try {
    $response = $dispatcher->dispatch(
        $_SERVER['REQUEST_METHOD'], 
        processInput($_SERVER['REQUEST_URI'])
    );
} catch (Phroute\Exception\HttpRouteNotFoundException $e) {
    var_dump($e);      
    die();

} catch (Phroute\Exception\HttpMethodNotAllowedException $e) {
    var_dump($e);       
    die();

}
